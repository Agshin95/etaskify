package eTaskify.demo.service.impl;

import eTaskify.demo.model.User;
import eTaskify.demo.repository.UserRepository;
import eTaskify.demo.service.RegisteredUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

@Service
@RequiredArgsConstructor
public class RegisteredUserServiceImpl implements RegisteredUserService {

    private final UserRepository userRepository;

    @Override
    public User getRegistered() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String email;
        if (principal instanceof UserDetails) {
            email = ((UserDetails) principal).getUsername();
        } else email = principal.toString();
        return userRepository.findByEmail(email).get();
    }
}
