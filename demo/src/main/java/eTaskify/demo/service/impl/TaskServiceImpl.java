package eTaskify.demo.service.impl;

import eTaskify.demo.model.dto.TaskRequestDto;
import eTaskify.demo.model.dto.TaskResponseDto;
import eTaskify.demo.service.TaskService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class TaskServiceImpl implements TaskService {
    @Override
    public TaskResponseDto create(TaskRequestDto taskRequestDto) {
        return null;
    }

    @Override
    public Page<TaskResponseDto> get(Pageable pageable) {
        return null;
    }
}
