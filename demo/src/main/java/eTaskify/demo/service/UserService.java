package eTaskify.demo.service;

import eTaskify.demo.model.dto.UserRequestDto;
import eTaskify.demo.model.dto.UserResponseDto;

public interface UserService {
    UserResponseDto create(UserRequestDto userRequestDto);
}
