package eTaskify.demo.service;

import eTaskify.demo.model.dto.OrganisationRequestDto;
import eTaskify.demo.model.dto.OrganisationResponseDto;

public interface OrganisationService {
    OrganisationResponseDto create(OrganisationRequestDto organisationRequestDto);
}
