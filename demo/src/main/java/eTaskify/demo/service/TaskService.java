package eTaskify.demo.service;

import eTaskify.demo.model.dto.TaskRequestDto;
import eTaskify.demo.model.dto.TaskResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TaskService {
    TaskResponseDto create(TaskRequestDto taskRequestDto);

    Page<TaskResponseDto> get(Pageable pageable);
}
