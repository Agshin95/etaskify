package eTaskify.demo.service;

import eTaskify.demo.model.User;

public interface RegisteredUserService {
    User  getRegistered();
}
