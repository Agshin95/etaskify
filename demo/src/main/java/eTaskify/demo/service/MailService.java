package eTaskify.demo.service;

import eTaskify.demo.model.dto.TaskRequestDto;

public interface MailService {
    void sendEmailToUsersAboutTask(TaskRequestDto task);
}
