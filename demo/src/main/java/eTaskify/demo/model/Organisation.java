package eTaskify.demo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Organisation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String organisationName;
    private String phone;
    private String address;
    @OneToMany(mappedBy = "organisation", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private List<User> users;
}
